#!/bin/bash

ATHENA_BRANCHES=21.3

initial_tdaq()
{
    # Initial checkout
    if [ ! -d ${SRC_ROOT}/tdaq-nightly ]; then
        mkdir -p ${SRC_ROOT}/tdaq-nightly
        cd ${SRC_ROOT}/tdaq-nightly
        git clone --recurse-submodules https://oauth2:${GITTOKEN}@gitlab.cern.ch/atlas-tdaq-software/tdaq-cmake.git tdaq-99-00-00
        git clone --recurse-submodules https://oauth2:${GITTOKEN}@gitlab.cern.ch/atlas-tdaq-software/tdaq-common-cmake.git tdaq-common-99-00-00
    fi

    # Add other branches added via git worktree
    if [ ! -d ${SRC_ROOT}/tdaq-08-02-01 ]; then
        mkdir -p ${SRC_ROOT}/tdaq-08-02-01
        (cd ${SRC_ROOT}/tdaq-nightly/tdaq-99-00-00; git worktree add ${SRC_ROOT}/tdaq-08-02-01/tdaq-08-02-01 origin/tdaq-08-02-01)
        (cd ${SRC_ROOT}/tdaq-nightly/tdaq-common-99-00-00; worktree add ${SRC_ROOT}/tdaq-08-02-01/tdaq-common-03-03-00 origin/tdaq-common-03-03-00)
    fi
}


update_tdaq()
{
    # Update, usually onlq this will run.
    for dir in ${SRC_ROOT}/tdaq-*/*
    do
        if [ -d $dir ]; then
            (cd $dir; git pull; git submodule update -i)
        fi
    done
}

initial_athena()
{
    # initial checkout
    if [ ! -d ${SRC_ROOT}/athena-master ]; then
        mkdir -p ${SRC_ROOT}/athena-master
        cd ${SRC_ROOT}/athena-master
        git clone https://gitlab.cern.ch/atlas/athena.git athena-master
    fi

    # Add other branches via git worktree 
    for br in 21.3
    do
        if [ ! -d ${SRC_ROOT}/athena-$br ]; then
            cd ${SRC_ROOT}/athena-master
            git worktree add ${SRC_ROOT}/athena-$br origin/$br
        fi
    done
}

update_athena()
{
    # Update
    for dir in ${SRC_ROOT}/athena-*
    do
        cd ${dir}
        git pull
    done
}

update()
{
    while true
    do
        if [ -w ${SRC_ROOT}/ ]; then
            if [ -n "${GITTOKEN}" ]; then
                update_tdaq
            fi
            update_athena
        fi
        sleep 600
    done
}

initial_tdaq
intitial_athena
update &
/scripts/start.sh
