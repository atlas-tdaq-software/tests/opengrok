FROM docker.io/opengrok/docker:1.2.7
MAINTAINER Reiner.Hauser@cern.ch
RUN chmod a+rwx /opengrok/data /var/opengrok /var/opengrok/etc /opengrok/src /var/run
COPY update.sh /scripts/update.sh
RUN chmod -R a+rxw /usr/local/tomcat
CMD  /scripts/update.sh
